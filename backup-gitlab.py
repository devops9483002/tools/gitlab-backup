import os
from datetime import datetime

import git
from gitlab import Gitlab
import tarfile


GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
GITLAB_URL = 'https://gitlab.com'
BACKUP_DIRECTORY = 'gitlab-backup'


def create_tgz(repo_path, repo_name):
    current_date = datetime.now().strftime('%Y-%m-%d')
    tgz_filename = f"{repo_name.replace('/', '_')}-{current_date}.tgz"

    with tarfile.open(tgz_filename, 'w:gz') as tar:
        tar.add(repo_path, arcname=os.path.basename(repo_path))


def clone_repos():
    gitlab = Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
    projects = gitlab.projects.list(all=True)

    if not os.path.exists(BACKUP_DIRECTORY):
        os.makedirs(BACKUP_DIRECTORY)

    for project in projects:
        repo_url = project.ssh_url_to_repo
        repo_name = project.path_with_namespace
        local_repo_path = os.path.join(BACKUP_DIRECTORY, repo_name)

        if os.path.exists(local_repo_path):
            print(f"Updating {repo_name}...")
            try:
                repo = git.Repo(local_repo_path)
                remote = repo.remote()
                remote.fetch()
                active_branch = repo.active_branch
                tracking_branch = active_branch.tracking_branch()
                
                if tracking_branch:
                    repo.git.reset('--hard', tracking_branch)
                    print(f"Updated {repo_name} successfully.")
                else:
                    print(f"Failed to update {repo_name}: tracking branch not found")
            except Exception as e:
                print(f"Failed to update {repo_name}: {e}")
        else:
            print(f"Cloning {repo_name}...")
            try:
                os.makedirs(os.path.dirname(local_repo_path), exist_ok=True)
                git.Repo.clone_from(repo_url, local_repo_path, env={"GIT_TERMINAL_PROMPT": "0"})
                print(f"Cloned {repo_name} successfully.")
            except Exception as e:
                print(f"Failed to clone {repo_name}: {e}")


def backup_snippets():
    gitlab = Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
    snippets = gitlab.snippets.list(all=True)

    snippets_backup_directory = os.path.join(BACKUP_DIRECTORY, 'snippets')
    if not os.path.exists(snippets_backup_directory):
        os.makedirs(snippets_backup_directory)

    for snippet in snippets:
        snippet_id = snippet.id
        snippet_title = snippet.title.replace("/", "_")
        snippet_file_name = f"{snippet_id}-{snippet_title}.txt"
        snippet_file_path = os.path.join(snippets_backup_directory, snippet_file_name)

        print(f"Backing up snippet {snippet_title}...")
        try:
            full_snippet = gitlab.snippets.get(snippet_id)
            snippet_description = full_snippet.description
            raw_snippet = full_snippet.content().decode('utf-8')

            with open(snippet_file_path, "w") as f:
                if snippet_description:
                    f.write(f"Description: {snippet_description}\n\n")
                f.write(raw_snippet)

            print(f"Backed up snippet {snippet_title} successfully.")
        except Exception as e:
            print(f"Failed to back up snippet {snippet_title}: {e}")


if __name__ == "__main__":
    clone_repos()
    backup_snippets()
    create_tgz(BACKUP_DIRECTORY, 'gitlab-backup')
